<?php 
session_start();
$option=$_POST['option'];

$move=array();
$move['left']=-90;
$move['right']=90;
$move['move']=1;

$orientation=array();
$orientation['N']=0;
$orientation['E']=90;
$orientation['S']=180;
$orientation['O']=270;

$robots=array();
if(array_key_exists('robots',$_SESSION)){
	$robots=$_SESSION['robots'];
}

function create(){
	global $_POST, $_SESSION, $robots, $orientation, $return;
	
	$IDbot=time();
	$origin=$_POST['position'];
	$position = explode(' ',$origin);
	
	if(count($position)==3){
		$robots[$IDbot]=array();
		$robots[$IDbot]['X']=$position[0];
		$robots[$IDbot]['Y']=$position[1];
		$robots[$IDbot]['O']=$orientation[$position[2]];
		$robots[$IDbot]['OS']=$position[2];
		
		$_SESSION['active']=$IDbot;
		$return['result']='successful';
		$return['robot']=$IDbot;
		$return['position']=$origin;
		$return['message']='Creado robots '.$IDbot;
		
	}else{
		$return['result']='error';
		$return['message']='Posicion '.$origin.', es incorrecta';
	}
}



function move(){ 
	global $_POST, $_SESSION, $robots, $orientation, $return, $move;
	
	$robot=$_POST['robot'];
	$active=$_SESSION['active'];
	$action=$_POST['action'];
	
	if(array_key_exists($active,$robots)){
		$O=$robots[$active]['O'];
		$OS=$robots[$active]['OS'];
		
		if($action=='move'){
			foreach($orientation as $key => $value){
				if($key==$OS){
					
					if($key == 'N'){
						$robots[$active]['Y']=$robots[$active]['Y']+$move[$action];
					
					}else if($key == 'S'){
						$robots[$active]['Y']=$robots[$active]['Y']-$move[$action];
					
					}else if($key == 'E'){
						$robots[$active]['X']=$robots[$active]['X']+$move[$action];
					
					}else if($key == 'O'){
						$robots[$active]['X']=$robots[$active]['X']-$move[$action];
					}
					
					break;
				}
			}
				
			$return['message']='Movimiento Adelante, realizado correctamente';
			
		}else if(array_key_exists($action,$move)){
			
			$O=$O+$move[$action];
			
			if($O<0){
				$O=$O+360;
				
			}else if($O>=360){
				$O=$O-360; 
			}
			
			foreach($orientation as $key => $value){
				if($value==$O){
					$robots[$active]['O']=$O;
					$robots[$active]['OS']=$key;
					break;
				}
			}
			
			$return['message']='Movimiento '.$robots[$active]['OS'].', realizado correctamente';
			
		}
		
		$return['result']='successful';
		$return['robot']=$active;
		$return['position']=$robots[$active]['X'].' '.$robots[$active]['Y'].' '.$robots[$active]['OS'];
		
	}else{
		$return['result']='error';
		$return['message']='Robot '.$active.', no existe';
	}
	
}



$return=array();
$return['result']='error';
$return['option']=$option;

if($option=='create'){
	create();
	
}else if($option=='move'){
	move();
	
}else{
	$return['message']='Opcion '.$option.', no esta configurada';
}

//ROBOTS
$_SESSION['robots']=$robots;
$return['robots']=$robots;
echo json_encode($return);



?>