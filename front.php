<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title>Test Enalquiler</title>
        
        <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        
        <script type="text/javascript">
		$(document).ready(function(){

			$('#create form').on('submit', function(e){
				e.preventDefault();
				
				var values=$(this).serializeArray();
					values.push({name: "option", value: "create"});
					
				var callback = function(response) {
						//console.log(response);
						$('#message .data').html(response['message']);
						$('#position .data').html(response['position']);
						//response = JSON.parse(response);
				  	}
					
				AjaxData('back.php', values, callback);
			}); 


			$('#move .controls button').on('click', function(e){
				e.preventDefault();
				
				var action = $(this).data('action'); 
				
				var values=[];
					values.push({name: "option", value: "move"});
					values.push({name: "action", value: action});
					
				var callback = function(response) {
						//console.log(response);
						$('#message .data').html(response['message']);
						$('#position .data').html(response['position']);
						//response = JSON.parse(response);
				  	}
				
				AjaxData('back.php', values, callback);
			}); 
		});
		
		
		function AjaxData(url, data, callback){
					
				$.ajax({
				  	type: 'POST',
				  	url: url,
				  	data: $.param(data),
				  	dataType: 'json',
				  	beforeSend: function(xhr) {
						$('#message .data').html('procesando');
				  	},
				  	success : callback
				});
		}
		</script>
    </head>

    <body>
        
        <div id="create">
            <h3 class="title">Crear Robot</h3>
            <form>
                <input type="text" name="position" value="" placeholder="X Y O">
                <button type="submit">Crear</button>
            </form>
        </div>
        
        <div id="move">
        	<h3 class="title">Controles</h3>
        	<div class="controls">
                <button type="button" data-action="left">Girar Izquierda</button>
                <button type="button" data-action="right">Girar Derecha</button>
                <button type="button" data-action="move">Mover</button>
            </div>
        </div>
    
        <div id="position">
        	<h3 class="title">Posición</h3>
        	<div class="data"></div>
        </div>
    
        <div id="message">
        	<h3 class="title">Mensaje</h3>
        	<div class="data"></div>
        </div>
        
        <div id="robots" style="display:none;">
        	<h3 class="title">Robots</h3>
        	<div class="list"></div>
        </div>
        
    </body>
</html>